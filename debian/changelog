wmnut (0.66-2) UNRELEASED; urgency=low

  * debian/control: Orphaning of wmnut, update VCS fields

 -- Arnaud Quette <aquette@debian.org>  Thu, 09 Jul 2020 10:10:46 +0200

wmnut (0.66-1) unstable; urgency=low

  * New upstream release
  * debian/rules: add missing dpkg flags
  * debian/control: update Standards-Version to 3.9.8

 -- Arnaud Quette <aquette@debian.org>  Sat, 24 Dec 2016 00:18:40 +0100

wmnut (0.65-1) unstable; urgency=low

  * New upstream release (Closes: #728002)
  * debian/copyright: update to comply to copyright format 1.0
  * debian/control
    - update Standards-Version to 3.9.6
    - update Homepage
    - add Vcs-Browser and Vcs-Git fields
  * debian/watch: update URL
  * debian/rules: general update and simplification
  
 -- Arnaud Quette <aquette@debian.org>  Fri, 23 Dec 2016 22:54:52 +0100

wmnut (0.64-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control: Adjust libupsclient build-dependency (Closes: #730400)

 -- Laurent Bigonville <bigon@debian.org>  Sun, 27 Apr 2014 22:18:00 +0200

wmnut (0.64-1) unstable; urgency=low

  * New upstream release (Closes: #586956, #671308, #565751)
  * debian/control:
    - update Standards-Version to 3.9.3
    - update debhelper to 9
    - remove dpatch, autoconf and automake Build-Depends
  * debian/rules:
    - fix Lintian warning debian-rules-missing-recommended-target
    - remove dpatch targets
  * debian/copyright: update GPL-2 reference
  * debian/patches/*: removed
  * debian/source.lintian-overrides: remove, since it's no longer needed
    with format 3.0
  * debian/compat: update to 9
  * debian/source/format: created, for 3.0 (quilt)
  * debian/copyright: update Copyright and download location
  * debian/watch: update URL

 -- Arnaud Quette <aquette@debian.org>  Tue, 05 Jun 2012 18:34:30 +0200

wmnut (0.62-4) unstable; urgency=low

  * debian/control:
    - add a Build-Depends on pkg-config, dpatch, autoconf and automake,
    - bump libupsclient1-dev Build-Depends to >= 2.4.0
    - bump the debhelper Build-Depends to (>= 6.0.7~) for dh_lintian
  * debian/rules:
    - add the needed rules for dpatch application, and call autoreconf to apply
      patches and update outdated-autotools-helper-file
    - add a call to dh_lintian for source.lintian-overrides
  * debian/source.lintian-overrides: added to cope with the lintian warning due
    to the dpatch + autoreconf call (patch-system-but-direct-changes-in-diff)
  * debian/patches/01_configure_use_pkgconfig.dpatch: created to fix the
    configure script not catching the new libupsclient location. The temporary
    fix is to only rely on pkg-config while waiting for a proper upstream fix.
  * debian/patches/02_upsconn_t.dpatch: move to the new NUT client struct name

 -- Arnaud Quette <aquette@debian.org>  Wed, 18 Mar 2009 11:03:55 +0100
 
wmnut (0.62-3) unstable; urgency=low

  * debian/control:
    - update Standards-Version to 3.8.0
    - update the Build-Depends on nut-dev to libupsclient1-dev (Closes:
      #490425)
    - update the Build-Depends on x-dev to x11proto-core-dev

 -- Arnaud Quette <aquette@debian.org>  Sun, 13 Jul 2008 14:36:05 +0200

wmnut (0.62-2) unstable; urgency=low

  * debian/control:
    - update Build-Depends for nut to 2.2.1 (Closes: #443003)
    - update Standards-Version to 3.7.3
    - move the Homepage from Description to the official field and update it
  * debian/watch: update to URL (Closes: #449946)
  * debian/rules:
    - fix debian-rules-ignores-make-clean-error
    - remove DH_COMPAT
  * debian/compat: created to complete the above fix

 -- Arnaud Quette <aquette@debian.org>  Thu, 24 Jan 2008 15:15:44 +0100

wmnut (0.62-1) unstable; urgency=low

  * New upstream release
  * Remove the libxi-dev build-depends as the XBell() calls have been disabled
    upstream. Recall the ignored bug close (closes: #346981)

 -- Arnaud Quette <aquette@debian.org>  Fri, 13 Jan 2006 14:54:13 +0100

wmnut (0.61-2) unstable; urgency=low

  * debian/control:
    - update build-depends for Xorg transition, and add libxi-dev for
    the XBell calls (closes: #346981)
    - update standard version to fix a lintian warning
    - update debhelper build-depends to >= 5.0.0
  * debian/rules: upgrade DH_COMPAT to 5

 -- Arnaud Quette <aquette@debian.org>  Mon,  9 Jan 2006 10:00:01 +0200

wmnut (0.61-1) unstable; urgency=low

  * New upstream release (closes: #291225)
  * debian/control: update Build-Depends for nut (2.0.1 release)

 -- Arnaud Quette <aquette@debian.org>  Mon, 14 Mar 2005 12:53:47 +0100

wmnut (0.60-4) unstable; urgency=low

  * debian/control: change Build-Depends from nut to nut-dev (closes:
    Bug#271120)
  * debian/watch: update 
    - to new format,
    - URL/Homepage and
    - NUT URL
    - Standards-Version
  * debian/copyright: updates and typo fix
  
 -- Arnaud Quette <aquette@debian.org>  Sat, 11 Sep 2004 21:48:22 +0200

wmnut (0.60-3) unstable; urgency=low

  * debian/control: upgrade nut deps to avoid nut {pre,post}inst errors

 -- Arnaud Quette <aquette@debian.org>  Sat, 15 Nov 2003 22:26:20 +0100
 
wmnut (0.60-2) unstable; urgency=low

  * debian/control: upgrade nut deps to have the missing parseconf.h (closes: Bug#220630)
  * debian/control: change a bit main description

 -- Arnaud Quette <aquette@debian.org>  Fri, 14 Nov 2003 17:26:20 +0100

wmnut (0.60-1) unstable; urgency=low

  * New upstream release
      * use the new libupsclient and autodetection for nut-1.4.1-pre3 (closes: Bug#216745)

 -- Arnaud Quette <aquette@debian.org>  Mon, 10 Nov 2003 13:26:20 +0100

wmnut (0.59-1) unstable; urgency=low

  * New upstream release
  * debian/control: changed Arnaud Quette address to the good Debian one
  * debian/control: update required version of nut
  * debian/control: add Homepage to Description

 -- Arnaud Quette <aquette@debian.org>  Sat, 18 Oct 2003 21:48:00 +0200

wmnut (0.58-1) unstable; urgency=low

  * New upstream release

 -- Arnaud Quette <arnaud.quette@free.fr>  Mon, 14 Jul 2003 15:13:21 +0200

wmnut (0.55-1) unstable; urgency=low

  * New upstream release (closes: Bug#182558)

 -- Arnaud Quette <arnaud.quette@free.fr>  Sat, 22 Mar 2003 22:47:44 +0100

wmnut (0.51-1) unstable; urgency=low

  * New upstream release
  * Corrected debian/control: switched to autoconf/automake
  * Corrected debian/control: updated Standards-Version to 3.5.8
  * Corrected debian/control: updated Build-Depends for nut to 
  	(>> 1.1.3) due to upsclient needs
  * Suppressed debian/dirs (not more needed with autoconf)
  * Modified debian/docs: include wmnutrc configuration file
  * Added debian/watch file

 -- Arnaud Quette <arnaud.quette@free.fr>  Tue, 12 Feb 2003 10:32:32 -0800

wmnut (0.10-2) unstable; urgency=low

  * applied -0.20-preHoneymoon patch ... will rev to 0.20 once 0.20 is
    released officially (closes: Bug#166273)

 -- Luca Filipozzi <lfilipoz@debian.org>  Tue, 19 Nov 2002 10:32:32 -0800

wmnut (0.10-1) unstable; urgency=low

  * New upstream release

 -- Luca Filipozzi <lfilipoz@debian.org>  Sun,  2 Jun 2002 21:03:35 -0700

wmnut (0.09-1) unstable; urgency=low

  * New upstream release

 -- Luca Filipozzi <lfilipoz@Debian.org>  Sat, 23 Mar 2002 19:52:46 -0800

wmnut (0.08-1) unstable; urgency=low

  * Initial Release.

 -- Luca Filipozzi <lfilipoz@debian.org>  Sun, 24 Feb 2002 15:36:56 -0800

